﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilitiesLibrary
{
    public class Utilities
    {
        /// <summary>
        /// Convert string to int
        /// </summary>
        /// <param name="input">String to convert</param>
        /// <param name="result">Result as int</param>
        /// <returns></returns>
        public static bool StringToInt(string input, out int result)
        {
            return (int.TryParse(input, out result));
        }

        /// <summary>
        /// Convert String to Double
        /// </summary>
        /// <param name="input">String to convert</param>
        /// <param name="result">Result as double</param>
        /// <returns></returns>
        public static bool StringToDouble(string input, out double result)
        {
            return (double.TryParse(input, out result));
        }

        /// <summary>
        /// Convert string to int within interval
        /// </summary>
        /// <param name="input">String to convert</param>
        /// <param name="result">Result as int</param>
        /// <param name="maxValue">Minimum accepted result</param>
        /// <param name="minValue">Maximum accepted result</param>
        /// <returns></returns>
        public static bool StringToIntWithinInterval(string input, out int result, int maxValue, int minValue)
        {
            if (int.TryParse(input, out result))
                if (result >= minValue || result <= maxValue)
                    return true;
            return false;
        }

        /// <summary>
        /// Convert string to double within interval
        /// </summary>
        /// <param name="input">String to convert</param>
        /// <param name="result">Result as double</param>
        /// <param name="maxValue">Mimimun accepted result</param>
        /// <param name="minValue">Maximum accepted result</param>
        /// <returns></returns>
        public static bool StringToDoubleWithinInterval(string input, out double result, double maxValue, double minValue)
        {
            if (double.TryParse(input, out result))
                if (result >= minValue || result <= maxValue)
                    return true;
            return false;
        }
    }
}
