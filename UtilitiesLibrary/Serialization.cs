﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.Security;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilitiesLibrary
{
    /// <summary>
    /// Class that handle all serialization
    /// </summary>
    public class Serialization
    {
        /// <summary>
        /// Serialize data to file as XML
        /// </summary>
        /// <typeparam name="T">Type of object to serialize</typeparam>
        /// <param name="filePath">File to save to</param>
        /// <param name="obj">object to serialize</param>
        public static void XMLSerialization<T>(string filePath, T obj, Type[] alltypes = null)
        {
            string error = "File not saved!";
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T), alltypes);
            StreamWriter writer = new StreamWriter(filePath);

            try
            {
                xmlSerializer.Serialize(writer, obj);
            }
            catch (SecurityException ex)
            {
                File.Delete(filePath);
                error += string.Format("\nPermission error: {0}", ex.Message);
                throw new MySerializationException(error);
            }
            catch (SerializationException ex)
            {
                File.Delete(filePath);
                error += string.Format("\nSerialization error:  {0}", ex.Message);
                throw new MySerializationException(error);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }
        
        /// <summary>
        /// Deserialize XML data from file
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static T XMLDeserialization<T>(string filePath, Type[] alltypes = null)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T), alltypes);
            StreamReader reader = new StreamReader(filePath);

            try
            {
                return (T)xmlSerializer.Deserialize(reader);
            }
            catch (SerializationException ex)
            {
                throw new MySerializationException("File is corrupt or not a valid program file"
                    + Environment.NewLine + "Error: " + ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                throw new MySerializationException("File is corrupt or not a valid program file"
                    + Environment.NewLine + "Error: " + ex.Message);
            }
            finally
            {
                if (reader != null) 
                    reader.Close();
            }
        }
        
        /// <summary>
        /// Binary serialize object to file
        /// </summary>
        /// <param name="filePath">File to save to</param>
        /// <param name="obj">Object to save to file</param>
        public static void BinarySerializeToFile<T>(string filePath, T obj)
        {
            FileStream fileStream = null;
            string error = "File not saved!";
            try
            {
                using (fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
                {
                    BinaryFormatter binaryFormatter = new BinaryFormatter();
                    binaryFormatter.Serialize(fileStream, obj);
                }
            }
            catch (SecurityException ex)
            {
                File.Delete(filePath);
                error += string.Format("\nPermission error: {0}", ex.Message);
                throw new MySerializationException(error);
            }
            catch (SerializationException ex)
            {
                File.Delete(filePath);
                error += string.Format("\nSerialization error:  {0}", ex.Message);
                throw new MySerializationException(error);
            }
            finally 
            {
                if (fileStream != null)
                    fileStream.Close();
            }
        }

        /// <summary>
        /// Binary deserialize data from file
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="file"></param>
        /// <returns></returns>
        public static T BinaryDeserializeFile<T>(string file)
        { 
            FileStream fileStream = null; 
            Object obj = null;

            try
            {
                fileStream = new FileStream(file, FileMode.Open);
                BinaryFormatter b = new BinaryFormatter();
                obj = b.Deserialize(fileStream);
            }
            catch (SerializationException ex) 
            {
                throw new MySerializationException("File is corrupt or not a valid program file" 
                    + Environment.NewLine + "Error: " + ex.Message);
            }
            finally 
            {
                if (fileStream != null)
                    fileStream.Close();    
            }
            return (T)obj;
        }
    }

    /// <summary>
    /// Enums of what type of serialization that is available
    /// </summary>
    public enum SerializationType
    { 
        Binary,
        XML
    }

    /// <summary>
    /// Class that handle serialization exceptions
    /// </summary>
    [Serializable]
    public class MySerializationException : Exception
    {
        public MySerializationException() { }
        public MySerializationException(string message) : base(message) { }
        public MySerializationException(string message, Exception inner) : base(message, inner) { }
        protected MySerializationException(
          SerializationInfo info,
          StreamingContext context)
            : base(info, context) { }
    }
}
