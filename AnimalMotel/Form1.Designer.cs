﻿namespace AnimalMotel
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.gbAnimalSpec = new System.Windows.Forms.GroupBox();
            this.pbSpecies = new System.Windows.Forms.PictureBox();
            this.gbSpeciesInfo = new System.Windows.Forms.GroupBox();
            this.cbSpeciesInfo = new System.Windows.Forms.CheckBox();
            this.cbListAll = new System.Windows.Forms.CheckBox();
            this.gbAnimalSpecification = new System.Windows.Forms.GroupBox();
            this.tbSpecifications = new System.Windows.Forms.TextBox();
            this.tbQuar = new System.Windows.Forms.TextBox();
            this.cbxQuar = new System.Windows.Forms.CheckBox();
            this.lblSpecificInfo = new System.Windows.Forms.Label();
            this.lblQuar = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.gbInfo = new System.Windows.Forms.GroupBox();
            this.tbInfo = new System.Windows.Forms.TextBox();
            this.gbAnimal = new System.Windows.Forms.GroupBox();
            this.lbAnimal = new System.Windows.Forms.ListBox();
            this.gbCategory = new System.Windows.Forms.GroupBox();
            this.lbCategory = new System.Windows.Forms.ListBox();
            this.gbGender = new System.Windows.Forms.GroupBox();
            this.lbGender = new System.Windows.Forms.ListBox();
            this.tbAge = new System.Windows.Forms.TextBox();
            this.lblAge = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.gbList = new System.Windows.Forms.GroupBox();
            this.btnDeleteAnimal = new System.Windows.Forms.Button();
            this.cbxShowOnly = new System.Windows.Forms.CheckBox();
            this.cbShowOnly = new System.Windows.Forms.ComboBox();
            this.lblListSpecial = new System.Windows.Forms.Label();
            this.animalDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.animalBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lbSpecial = new System.Windows.Forms.ListBox();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.jojoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileNew = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileSave = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuFileXML = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileXMLImport = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileXMLExport = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.animalBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.animalBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.gbAnimalSpec.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSpecies)).BeginInit();
            this.gbSpeciesInfo.SuspendLayout();
            this.gbAnimalSpecification.SuspendLayout();
            this.gbInfo.SuspendLayout();
            this.gbAnimal.SuspendLayout();
            this.gbCategory.SuspendLayout();
            this.gbGender.SuspendLayout();
            this.gbList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.animalDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.animalBindingSource)).BeginInit();
            this.menu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.animalBindingNavigator)).BeginInit();
            this.animalBindingNavigator.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbAnimalSpec
            // 
            this.gbAnimalSpec.Controls.Add(this.pbSpecies);
            this.gbAnimalSpec.Controls.Add(this.gbSpeciesInfo);
            this.gbAnimalSpec.Controls.Add(this.cbListAll);
            this.gbAnimalSpec.Controls.Add(this.gbAnimalSpecification);
            this.gbAnimalSpec.Controls.Add(this.btnAdd);
            this.gbAnimalSpec.Controls.Add(this.gbInfo);
            this.gbAnimalSpec.Controls.Add(this.gbAnimal);
            this.gbAnimalSpec.Controls.Add(this.gbCategory);
            this.gbAnimalSpec.Controls.Add(this.gbGender);
            this.gbAnimalSpec.Controls.Add(this.tbAge);
            this.gbAnimalSpec.Controls.Add(this.lblAge);
            this.gbAnimalSpec.Controls.Add(this.tbName);
            this.gbAnimalSpec.Controls.Add(this.lblName);
            this.gbAnimalSpec.Location = new System.Drawing.Point(12, 36);
            this.gbAnimalSpec.Name = "gbAnimalSpec";
            this.gbAnimalSpec.Size = new System.Drawing.Size(707, 229);
            this.gbAnimalSpec.TabIndex = 0;
            this.gbAnimalSpec.TabStop = false;
            this.gbAnimalSpec.Text = "Animal specifications";
            // 
            // pbSpecies
            // 
            this.pbSpecies.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pbSpecies.ErrorImage")));
            this.pbSpecies.Location = new System.Drawing.Point(518, 161);
            this.pbSpecies.Name = "pbSpecies";
            this.pbSpecies.Size = new System.Drawing.Size(60, 62);
            this.pbSpecies.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbSpecies.TabIndex = 12;
            this.pbSpecies.TabStop = false;
            // 
            // gbSpeciesInfo
            // 
            this.gbSpeciesInfo.Controls.Add(this.cbSpeciesInfo);
            this.gbSpeciesInfo.Location = new System.Drawing.Point(341, 180);
            this.gbSpeciesInfo.Name = "gbSpeciesInfo";
            this.gbSpeciesInfo.Size = new System.Drawing.Size(152, 43);
            this.gbSpeciesInfo.TabIndex = 11;
            this.gbSpeciesInfo.TabStop = false;
            this.gbSpeciesInfo.Text = "Species Info";
            this.gbSpeciesInfo.Visible = false;
            // 
            // cbSpeciesInfo
            // 
            this.cbSpeciesInfo.AutoSize = true;
            this.cbSpeciesInfo.Location = new System.Drawing.Point(10, 16);
            this.cbSpeciesInfo.Name = "cbSpeciesInfo";
            this.cbSpeciesInfo.Size = new System.Drawing.Size(44, 17);
            this.cbSpeciesInfo.TabIndex = 0;
            this.cbSpeciesInfo.Text = "Info";
            this.cbSpeciesInfo.UseVisualStyleBackColor = true;
            // 
            // cbListAll
            // 
            this.cbListAll.AutoSize = true;
            this.cbListAll.Location = new System.Drawing.Point(369, 160);
            this.cbListAll.Name = "cbListAll";
            this.cbListAll.Size = new System.Drawing.Size(93, 17);
            this.cbListAll.TabIndex = 10;
            this.cbListAll.Text = "List all animals";
            this.cbListAll.UseVisualStyleBackColor = true;
            this.cbListAll.CheckedChanged += new System.EventHandler(this.cbListAll_CheckedChanged);
            // 
            // gbAnimalSpecification
            // 
            this.gbAnimalSpecification.Controls.Add(this.tbSpecifications);
            this.gbAnimalSpecification.Controls.Add(this.tbQuar);
            this.gbAnimalSpecification.Controls.Add(this.cbxQuar);
            this.gbAnimalSpecification.Controls.Add(this.lblSpecificInfo);
            this.gbAnimalSpecification.Controls.Add(this.lblQuar);
            this.gbAnimalSpecification.Location = new System.Drawing.Point(13, 161);
            this.gbAnimalSpecification.Name = "gbAnimalSpecification";
            this.gbAnimalSpecification.Size = new System.Drawing.Size(297, 60);
            this.gbAnimalSpecification.TabIndex = 9;
            this.gbAnimalSpecification.TabStop = false;
            this.gbAnimalSpecification.Text = "Mammal Specifications";
            // 
            // tbSpecifications
            // 
            this.tbSpecifications.Location = new System.Drawing.Point(88, 16);
            this.tbSpecifications.Name = "tbSpecifications";
            this.tbSpecifications.Size = new System.Drawing.Size(29, 20);
            this.tbSpecifications.TabIndex = 4;
            // 
            // tbQuar
            // 
            this.tbQuar.Enabled = false;
            this.tbQuar.Location = new System.Drawing.Point(217, 36);
            this.tbQuar.Name = "tbQuar";
            this.tbQuar.Size = new System.Drawing.Size(21, 20);
            this.tbQuar.TabIndex = 3;
            // 
            // cbxQuar
            // 
            this.cbxQuar.AutoSize = true;
            this.cbxQuar.Location = new System.Drawing.Point(7, 37);
            this.cbxQuar.Name = "cbxQuar";
            this.cbxQuar.Size = new System.Drawing.Size(108, 17);
            this.cbxQuar.TabIndex = 2;
            this.cbxQuar.Text = "Under quarantine";
            this.cbxQuar.UseVisualStyleBackColor = true;
            this.cbxQuar.CheckedChanged += new System.EventHandler(this.cbxQuar_CheckedChanged);
            // 
            // lblSpecificInfo
            // 
            this.lblSpecificInfo.AutoSize = true;
            this.lblSpecificInfo.Location = new System.Drawing.Point(6, 19);
            this.lblSpecificInfo.Name = "lblSpecificInfo";
            this.lblSpecificInfo.Size = new System.Drawing.Size(63, 13);
            this.lblSpecificInfo.TabIndex = 1;
            this.lblSpecificInfo.Text = "No. of teeth";
            // 
            // lblQuar
            // 
            this.lblQuar.AutoSize = true;
            this.lblQuar.Location = new System.Drawing.Point(116, 38);
            this.lblQuar.Name = "lblQuar";
            this.lblQuar.Size = new System.Drawing.Size(95, 13);
            this.lblQuar.TabIndex = 0;
            this.lblQuar.Text = "Days in quarantine";
            // 
            // btnAdd
            // 
            this.btnAdd.Enabled = false;
            this.btnAdd.Location = new System.Drawing.Point(584, 161);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(109, 60);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.Text = "Add this new animal";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // gbInfo
            // 
            this.gbInfo.Controls.Add(this.tbInfo);
            this.gbInfo.Location = new System.Drawing.Point(518, 14);
            this.gbInfo.Name = "gbInfo";
            this.gbInfo.Size = new System.Drawing.Size(175, 141);
            this.gbInfo.TabIndex = 7;
            this.gbInfo.TabStop = false;
            this.gbInfo.Text = "Important info";
            // 
            // tbInfo
            // 
            this.tbInfo.Location = new System.Drawing.Point(7, 20);
            this.tbInfo.Multiline = true;
            this.tbInfo.Name = "tbInfo";
            this.tbInfo.Size = new System.Drawing.Size(162, 108);
            this.tbInfo.TabIndex = 0;
            // 
            // gbAnimal
            // 
            this.gbAnimal.Controls.Add(this.lbAnimal);
            this.gbAnimal.Location = new System.Drawing.Point(341, 14);
            this.gbAnimal.Name = "gbAnimal";
            this.gbAnimal.Size = new System.Drawing.Size(152, 141);
            this.gbAnimal.TabIndex = 6;
            this.gbAnimal.TabStop = false;
            this.gbAnimal.Text = "Animal";
            // 
            // lbAnimal
            // 
            this.lbAnimal.FormattingEnabled = true;
            this.lbAnimal.Location = new System.Drawing.Point(7, 20);
            this.lbAnimal.Name = "lbAnimal";
            this.lbAnimal.Size = new System.Drawing.Size(139, 108);
            this.lbAnimal.TabIndex = 0;
            this.lbAnimal.SelectedIndexChanged += new System.EventHandler(this.lbAnimal_SelectedIndexChanged);
            // 
            // gbCategory
            // 
            this.gbCategory.Controls.Add(this.lbCategory);
            this.gbCategory.Location = new System.Drawing.Point(170, 14);
            this.gbCategory.Name = "gbCategory";
            this.gbCategory.Size = new System.Drawing.Size(140, 141);
            this.gbCategory.TabIndex = 5;
            this.gbCategory.TabStop = false;
            this.gbCategory.Text = "Category";
            // 
            // lbCategory
            // 
            this.lbCategory.FormattingEnabled = true;
            this.lbCategory.Location = new System.Drawing.Point(7, 20);
            this.lbCategory.Name = "lbCategory";
            this.lbCategory.Size = new System.Drawing.Size(127, 108);
            this.lbCategory.TabIndex = 0;
            this.lbCategory.SelectedIndexChanged += new System.EventHandler(this.lbCategory_SelectedIndexChanged);
            // 
            // gbGender
            // 
            this.gbGender.Controls.Add(this.lbGender);
            this.gbGender.Location = new System.Drawing.Point(13, 85);
            this.gbGender.Name = "gbGender";
            this.gbGender.Size = new System.Drawing.Size(135, 70);
            this.gbGender.TabIndex = 4;
            this.gbGender.TabStop = false;
            this.gbGender.Text = "Gender";
            // 
            // lbGender
            // 
            this.lbGender.FormattingEnabled = true;
            this.lbGender.Location = new System.Drawing.Point(7, 20);
            this.lbGender.Name = "lbGender";
            this.lbGender.Size = new System.Drawing.Size(122, 43);
            this.lbGender.TabIndex = 0;
            // 
            // tbAge
            // 
            this.tbAge.Location = new System.Drawing.Point(48, 60);
            this.tbAge.Name = "tbAge";
            this.tbAge.Size = new System.Drawing.Size(100, 20);
            this.tbAge.TabIndex = 3;
            // 
            // lblAge
            // 
            this.lblAge.AutoSize = true;
            this.lblAge.Location = new System.Drawing.Point(10, 60);
            this.lblAge.Name = "lblAge";
            this.lblAge.Size = new System.Drawing.Size(26, 13);
            this.lblAge.TabIndex = 2;
            this.lblAge.Text = "Age";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(48, 22);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(100, 20);
            this.tbName.TabIndex = 1;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(7, 25);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(35, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Name";
            // 
            // gbList
            // 
            this.gbList.Controls.Add(this.btnDeleteAnimal);
            this.gbList.Controls.Add(this.cbxShowOnly);
            this.gbList.Controls.Add(this.cbShowOnly);
            this.gbList.Controls.Add(this.lblListSpecial);
            this.gbList.Controls.Add(this.animalDataGridView);
            this.gbList.Controls.Add(this.lbSpecial);
            this.gbList.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.gbList.Location = new System.Drawing.Point(13, 271);
            this.gbList.Name = "gbList";
            this.gbList.Size = new System.Drawing.Size(706, 212);
            this.gbList.TabIndex = 1;
            this.gbList.TabStop = false;
            this.gbList.Text = "List of registered animals with general data";
            // 
            // btnDeleteAnimal
            // 
            this.btnDeleteAnimal.Location = new System.Drawing.Point(229, 185);
            this.btnDeleteAnimal.Name = "btnDeleteAnimal";
            this.btnDeleteAnimal.Size = new System.Drawing.Size(130, 23);
            this.btnDeleteAnimal.TabIndex = 7;
            this.btnDeleteAnimal.Text = "Delete selected entry";
            this.btnDeleteAnimal.UseVisualStyleBackColor = true;
            this.btnDeleteAnimal.Click += new System.EventHandler(this.btnDeleteAnimal_Click);
            // 
            // cbxShowOnly
            // 
            this.cbxShowOnly.AutoSize = true;
            this.cbxShowOnly.Location = new System.Drawing.Point(8, 188);
            this.cbxShowOnly.Name = "cbxShowOnly";
            this.cbxShowOnly.Size = new System.Drawing.Size(75, 17);
            this.cbxShowOnly.TabIndex = 6;
            this.cbxShowOnly.Text = "Show only";
            this.cbxShowOnly.UseVisualStyleBackColor = true;
            this.cbxShowOnly.CheckedChanged += new System.EventHandler(this.cbxShowOnly_CheckedChanged);
            // 
            // cbShowOnly
            // 
            this.cbShowOnly.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbShowOnly.Enabled = false;
            this.cbShowOnly.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbShowOnly.FormattingEnabled = true;
            this.cbShowOnly.Location = new System.Drawing.Point(84, 185);
            this.cbShowOnly.Name = "cbShowOnly";
            this.cbShowOnly.Size = new System.Drawing.Size(65, 21);
            this.cbShowOnly.TabIndex = 5;
            this.cbShowOnly.SelectedIndexChanged += new System.EventHandler(this.cbShowOnly_SelectedIndexChanged);
            // 
            // lblListSpecial
            // 
            this.lblListSpecial.AutoSize = true;
            this.lblListSpecial.Location = new System.Drawing.Point(368, 18);
            this.lblListSpecial.Name = "lblListSpecial";
            this.lblListSpecial.Size = new System.Drawing.Size(66, 13);
            this.lblListSpecial.TabIndex = 2;
            this.lblListSpecial.Text = "Special data";
            // 
            // animalDataGridView
            // 
            this.animalDataGridView.AllowUserToAddRows = false;
            this.animalDataGridView.AllowUserToDeleteRows = false;
            this.animalDataGridView.AllowUserToResizeColumns = false;
            this.animalDataGridView.AllowUserToResizeRows = false;
            this.animalDataGridView.AutoGenerateColumns = false;
            this.animalDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.animalDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.animalDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.animalDataGridView.DataSource = this.animalBindingSource;
            this.animalDataGridView.Location = new System.Drawing.Point(8, 21);
            this.animalDataGridView.MultiSelect = false;
            this.animalDataGridView.Name = "animalDataGridView";
            this.animalDataGridView.ReadOnly = true;
            this.animalDataGridView.RowHeadersVisible = false;
            this.animalDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.animalDataGridView.Size = new System.Drawing.Size(351, 158);
            this.animalDataGridView.TabIndex = 4;
            this.animalDataGridView.SelectionChanged += new System.EventHandler(this.animalDataGridView_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.Width = 40;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn5.HeaderText = "Name";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Age";
            this.dataGridViewTextBoxColumn2.HeaderText = "Age";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn2.Width = 40;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Gender";
            this.dataGridViewTextBoxColumn3.HeaderText = "Gender";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn3.Width = 75;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Category";
            this.dataGridViewTextBoxColumn4.HeaderText = "Category";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn4.Width = 75;
            // 
            // animalBindingSource
            // 
            this.animalBindingSource.DataSource = typeof(AnimalMotel.DAL.Animal);
            // 
            // lbSpecial
            // 
            this.lbSpecial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbSpecial.Enabled = false;
            this.lbSpecial.FormattingEnabled = true;
            this.lbSpecial.Location = new System.Drawing.Point(368, 34);
            this.lbSpecial.Name = "lbSpecial";
            this.lbSpecial.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbSpecial.Size = new System.Drawing.Size(332, 145);
            this.lbSpecial.TabIndex = 3;
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.jojoToolStripMenuItem});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(739, 24);
            this.menu.TabIndex = 2;
            this.menu.Text = "menuStrip1";
            // 
            // jojoToolStripMenuItem
            // 
            this.jojoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileNew,
            this.menuFileOpen,
            this.menuFileSave,
            this.menuFileSaveAs,
            this.toolStripSeparator1,
            this.menuFileXML,
            this.toolStripSeparator2,
            this.menuFileExit});
            this.jojoToolStripMenuItem.Name = "jojoToolStripMenuItem";
            this.jojoToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.jojoToolStripMenuItem.Text = "File";
            // 
            // menuFileNew
            // 
            this.menuFileNew.Name = "menuFileNew";
            this.menuFileNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.menuFileNew.Size = new System.Drawing.Size(184, 22);
            this.menuFileNew.Text = "New";
            this.menuFileNew.Click += new System.EventHandler(this.menuFileNew_Click);
            // 
            // menuFileOpen
            // 
            this.menuFileOpen.Name = "menuFileOpen";
            this.menuFileOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.menuFileOpen.Size = new System.Drawing.Size(184, 22);
            this.menuFileOpen.Text = "Open";
            this.menuFileOpen.Click += new System.EventHandler(this.menuFileOpen_Click);
            // 
            // menuFileSave
            // 
            this.menuFileSave.Name = "menuFileSave";
            this.menuFileSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.menuFileSave.Size = new System.Drawing.Size(184, 22);
            this.menuFileSave.Text = "Save";
            this.menuFileSave.Click += new System.EventHandler(this.menuFileSave_Click);
            // 
            // menuFileSaveAs
            // 
            this.menuFileSaveAs.Name = "menuFileSaveAs";
            this.menuFileSaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.menuFileSaveAs.Size = new System.Drawing.Size(184, 22);
            this.menuFileSaveAs.Text = "Save as";
            this.menuFileSaveAs.Click += new System.EventHandler(this.menuFileSaveAs_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(181, 6);
            // 
            // menuFileXML
            // 
            this.menuFileXML.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileXMLImport,
            this.menuFileXMLExport});
            this.menuFileXML.Name = "menuFileXML";
            this.menuFileXML.Size = new System.Drawing.Size(184, 22);
            this.menuFileXML.Text = "XML";
            // 
            // menuFileXMLImport
            // 
            this.menuFileXMLImport.Name = "menuFileXMLImport";
            this.menuFileXMLImport.Size = new System.Drawing.Size(187, 22);
            this.menuFileXMLImport.Text = "Import from XML-file";
            this.menuFileXMLImport.Click += new System.EventHandler(this.menuFileXMLImport_Click);
            // 
            // menuFileXMLExport
            // 
            this.menuFileXMLExport.Name = "menuFileXMLExport";
            this.menuFileXMLExport.Size = new System.Drawing.Size(187, 22);
            this.menuFileXMLExport.Text = "Export to XML-file";
            this.menuFileXMLExport.Click += new System.EventHandler(this.menuFileXMLExport_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(181, 6);
            // 
            // menuFileExit
            // 
            this.menuFileExit.Name = "menuFileExit";
            this.menuFileExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.X)));
            this.menuFileExit.Size = new System.Drawing.Size(184, 22);
            this.menuFileExit.Text = "Exit";
            this.menuFileExit.Click += new System.EventHandler(this.menuFileExit_Click);
            // 
            // animalBindingNavigator
            // 
            this.animalBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.animalBindingNavigator.BindingSource = this.animalBindingSource;
            this.animalBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.animalBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.animalBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.animalBindingNavigatorSaveItem});
            this.animalBindingNavigator.Location = new System.Drawing.Point(0, 24);
            this.animalBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.animalBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.animalBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.animalBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.animalBindingNavigator.Name = "animalBindingNavigator";
            this.animalBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.animalBindingNavigator.Size = new System.Drawing.Size(739, 25);
            this.animalBindingNavigator.TabIndex = 3;
            this.animalBindingNavigator.Text = "bindingNavigator1";
            this.animalBindingNavigator.Visible = false;
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // animalBindingNavigatorSaveItem
            // 
            this.animalBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.animalBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("animalBindingNavigatorSaveItem.Image")));
            this.animalBindingNavigatorSaveItem.Name = "animalBindingNavigatorSaveItem";
            this.animalBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.animalBindingNavigatorSaveItem.Text = "Save Data";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 488);
            this.Controls.Add(this.animalBindingNavigator);
            this.Controls.Add(this.gbList);
            this.Controls.Add(this.gbAnimalSpec);
            this.Controls.Add(this.menu);
            this.MainMenuStrip = this.menu;
            this.Name = "Form1";
            this.Text = " ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.gbAnimalSpec.ResumeLayout(false);
            this.gbAnimalSpec.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSpecies)).EndInit();
            this.gbSpeciesInfo.ResumeLayout(false);
            this.gbSpeciesInfo.PerformLayout();
            this.gbAnimalSpecification.ResumeLayout(false);
            this.gbAnimalSpecification.PerformLayout();
            this.gbInfo.ResumeLayout(false);
            this.gbInfo.PerformLayout();
            this.gbAnimal.ResumeLayout(false);
            this.gbCategory.ResumeLayout(false);
            this.gbGender.ResumeLayout(false);
            this.gbList.ResumeLayout(false);
            this.gbList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.animalDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.animalBindingSource)).EndInit();
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.animalBindingNavigator)).EndInit();
            this.animalBindingNavigator.ResumeLayout(false);
            this.animalBindingNavigator.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbAnimalSpec;
        private System.Windows.Forms.TextBox tbAge;
        private System.Windows.Forms.Label lblAge;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.GroupBox gbGender;
        private System.Windows.Forms.ListBox lbGender;
        private System.Windows.Forms.GroupBox gbInfo;
        private System.Windows.Forms.GroupBox gbAnimal;
        private System.Windows.Forms.GroupBox gbCategory;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox gbList;
        private System.Windows.Forms.Label lblListSpecial;
        private System.Windows.Forms.ListBox lbSpecial;
        private System.Windows.Forms.ListBox lbAnimal;
        private System.Windows.Forms.ListBox lbCategory;
        private System.Windows.Forms.TextBox tbInfo;
        private System.Windows.Forms.GroupBox gbAnimalSpecification;
        private System.Windows.Forms.CheckBox cbxQuar;
        private System.Windows.Forms.Label lblSpecificInfo;
        private System.Windows.Forms.Label lblQuar;
        private System.Windows.Forms.TextBox tbSpecifications;
        private System.Windows.Forms.TextBox tbQuar;
        private System.Windows.Forms.CheckBox cbListAll;
        private System.Windows.Forms.GroupBox gbSpeciesInfo;
        private System.Windows.Forms.CheckBox cbSpeciesInfo;
        private System.Windows.Forms.PictureBox pbSpecies;
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem jojoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuFileNew;
        private System.Windows.Forms.ToolStripMenuItem menuFileOpen;
        private System.Windows.Forms.ToolStripMenuItem menuFileSave;
        private System.Windows.Forms.ToolStripMenuItem menuFileSaveAs;
        private System.Windows.Forms.ToolStripMenuItem menuFileXML;
        private System.Windows.Forms.ToolStripMenuItem menuFileXMLImport;
        private System.Windows.Forms.ToolStripMenuItem menuFileXMLExport;
        private System.Windows.Forms.ToolStripMenuItem menuFileExit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.BindingSource animalBindingSource;
        private System.Windows.Forms.BindingNavigator animalBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton animalBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView animalDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.ComboBox cbShowOnly;
        private System.Windows.Forms.CheckBox cbxShowOnly;
        private System.Windows.Forms.Button btnDeleteAnimal;
    }
}

