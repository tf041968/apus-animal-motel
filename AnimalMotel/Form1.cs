﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AnimalLibrary;
using PictrureLibrary;
using UtilitiesLibrary;
using AnimalMotel.DAL;

namespace AnimalMotel
{
    public partial class Form1 : Form
    {
        private int age;
        private int quarantineDays;
        private double specificationInfo;
        AnimalManager animalManager;
        public Form1()
        {
            InitializeComponent();
            animalManager = new AnimalManager();
            InitializeGUI();
        }

        /// <summary>
        /// Initialize GUI
        /// </summary>
        private void InitializeGUI()
        {
            this.Text = "Apus Animal Motel";
            cbShowOnly.DataSource = animalManager.GetEnum(typeof(Category));//Enum.GetValues(typeof(Category));
            LoadDatabase();
            animalManager.FileToSave = string.Empty;
            SetListBoxContent();
        }

        /// <summary>
        /// Populates Category and gender listbox
        /// </summary>
        private void SetListBoxContent()
        {
            CategoryControl();
            GenderControl();
            lbSpecial.Items.Clear();
        }

        /// <summary>
        /// Show gender enum in listbox
        /// </summary>
        private void GenderControl()
        {
            lbGender.DataSource = animalManager.GetEnum(typeof(Gender));//Enum.GetValues(typeof(Gender));
            lbGender.SelectedIndex = 0;
        }

        /// <summary>
        /// Show category enum in listbox
        /// </summary>
        private void CategoryControl()
        {
            lbCategory.DataSource = animalManager.GetEnum(typeof(Category));//Enum.GetValues(typeof(Category));
            lbCategory.SelectedIndex = 0;
        }

        /// <summary>
        /// When user clicks add animal button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (GuiCheck())
            {
                try
                {
                    animalManager.CreateAnimal(lbCategory.SelectedIndex, lbAnimal.SelectedIndex,
                        specificationInfo, quarantineDays, cbSpeciesInfo.Checked);
                    animalManager.SetStandardData(tbName.Text, age, (Gender)lbGender.SelectedIndex, tbInfo.Text);
                    LoadDatabase();
                    ClearTextboxes();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        /// <summary>
        /// Validates user input
        /// </summary>
        /// <returns>true or false</returns>
        public bool GuiCheck()
        {
            string messageTitle = "Incomplete input!";
            if (string.IsNullOrEmpty(tbName.Text))
            {
                MessageBox.Show("You must enter a name", messageTitle, 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                tbName.Focus();
                return false;
            }
            else if (!Utilities.StringToInt(tbAge.Text, out age) || age < 0)
            {
                MessageBox.Show("You must enter age, only digits and it has to be a positive number", messageTitle, 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                tbAge.Focus();
                return false;
            }
            else if (!string.IsNullOrEmpty(tbSpecifications.Text) && 
                !Utilities.StringToDouble(tbSpecifications.Text, out specificationInfo))
            {
                MessageBox.Show("You have to enter the special information as digits and use comma if decimal", 
                    messageTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                tbSpecifications.Focus();
                return false;
            }
            else if (!string.IsNullOrEmpty(tbQuar.Text) && !Utilities.StringToInt(tbQuar.Text, out quarantineDays))
            {
                MessageBox.Show("Days in quarantine can only be numbers", messageTitle, 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                tbQuar.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(tbQuar.Text) || quarantineDays < 0)
                quarantineDays = 0;
            return true;
        }

        /// <summary>
        /// Clear all textboxes
        /// </summary>
        private void ClearTextboxes()
        {
            tbAge.Text = string.Empty;
            tbInfo.Text = string.Empty;
            tbName.Text = string.Empty;
            tbQuar.Text = string.Empty;
            tbSpecifications.Text = string.Empty;
        }

        /// <summary>
        /// Fires when selection is changed in Category listbox
        /// Updates Animal listbox so that the correct animals are shown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            ///Only update animal listbox if "List all animals" box is not checked. 
            if (!cbListAll.Checked)
            {
                switch ((Category)lbCategory.SelectedIndex)
                {
                    case Category.Bird:
                        lbAnimal.DataSource = animalManager.GetEnum(typeof(BirdSpecies));//Enum.GetValues(typeof(BirdSpecies));
                        break;
                    case Category.Insect:
                        lbAnimal.DataSource = animalManager.GetEnum(typeof(InsectSpecies));//Enum.GetValues(typeof(InsectSpecies));
                        break;
                    case Category.Mammal:
                        lbAnimal.DataSource = animalManager.GetEnum(typeof(MammalSpecies));//Enum.GetValues(typeof(MammalSpecies));
                        break;
                    case Category.Marine:
                        lbAnimal.DataSource = animalManager.GetEnum(typeof(MarineSpecies));//Enum.GetValues(typeof(MarineSpecies));
                        break;
                    //HAS NOT IMPLEMENTED ANIMALS FOR THESE CATEGORIES YET
                    //COMMENTED OUT CODE BELOW IN CASE I WANT TO INPLEMENT IT IN THE FUTURE
                    //case (int)Category.Reptile:
                    //    lbAnimal.DataSource = Enum.GetValues(typeof(ReptileSpecies));
                    //    lblSpecificInfo.Text = "Length";
                    //    break;
                }
                if ((Category)lbCategory.SelectedIndex == Category.Mammal)
                    gbAnimalSpecification.Visible = true;
                lbAnimal.SelectedIndex = -1;
                SelectSpeciesCategory();
                ShowHideQuarantineGUI();
            }
        }

        /// <summary>
        /// Show Category image.
        /// </summary>
        /// <param name="species">Category name in lower case</param>
        private void ShowImage(string species)
        {
            AnimalImage image = new AnimalImage();
            try
            {
                pbSpecies.Image = image.GetImageForCategory(species);
            }
            catch (CategoryNotFoundExcetion)
            {
                pbSpecies.Image = pbSpecies.ErrorImage;
            }
        }

        /// <summary>
        /// Enable/Disable add button
        /// Change species info text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbAnimal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbAnimal.SelectedIndex == -1)
                btnAdd.Enabled = false;
            else
            {
                btnAdd.Enabled = true;
                cbSpeciesInfo.Text = lbAnimal.SelectedItem.ToString();
            }

            if (lbAnimal.SelectedIndex != -1)
            {
                switch ((MammalSpecies)lbAnimal.SelectedItem)
                {
                    case MammalSpecies.Cat:
                        cbSpeciesInfo.Text = "Has no tail";
                        break;
                    case MammalSpecies.Dog:
                        cbSpeciesInfo.Text = "Has rabies";
                        break;
                }
            }
            if (cbListAll.Checked)
            {
                SelectSpeciesCategory();
                ShowHideQuarantineGUI();
            }
            animalManager.LatestCategory = lbCategory.SelectedIndex;
        }

        /// <summary>
        /// Check if quarantine checkbox is ticked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxQuar_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxQuar.Checked)
                tbQuar.Enabled = true;
            else
                tbQuar.Enabled = false;
        }

        /// <summary>
        /// Show all animals in list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbListAll_CheckedChanged(object sender, EventArgs e)
        {
            if (cbListAll.Checked)
            {
                animalManager.LatestCategory = lbCategory.SelectedIndex;
                lbCategory.Enabled = false;
                lbAnimal.DataSource = null;
                lbAnimal.Items.Clear();
                lbAnimal.Items.AddRange(AnimalManager.GetEnumList().ToArray());
            }
            else
            {
                int speciesIndex = 0;
                if (lbAnimal.SelectedIndex >= 0)
                    speciesIndex = (int)lbAnimal.SelectedItem;
                lbCategory.Enabled = true;
                lbAnimal.DataSource = null;
                lbAnimal.Items.Clear();
                lbCategory.Update();
                lbCategory.SelectedIndex = -1;
                lbCategory.SelectedIndex = animalManager.LatestCategory;
                SetSpecificDataLabels(lbCategory.SelectedItem.ToString());
                lbAnimal.SelectedIndex = speciesIndex;
            }
        }

        /// <summary>
        /// Selects correct species category for the selected animal.
        /// Used when "List all animals" box is checked. 
        /// </summary>
        private void SelectSpeciesCategory()
        {
            if (lbAnimal.SelectedItem != null)
            {
                var species = lbAnimal.SelectedItem.GetType().Name;
                switch (species)
                {
                    case "BirdSpecies":
                        lbCategory.SelectedIndex = (int)Category.Bird;
                        break;
                    case "InsectSpecies":
                        lbCategory.SelectedIndex = (int)Category.Insect;
                        break;
                    case "MammalSpecies":
                        lbCategory.SelectedIndex = (int)Category.Mammal;
                        break;
                }
            }
            SetSpecificDataLabels(lbCategory.SelectedItem.ToString());
        }

        /// <summary>
        /// Set labels and groubox descriptions to match chosen species
        /// </summary>
        /// <param name="animalSpecies"></param>
        public void SetSpecificDataLabels(string animalSpecies)
        {
            string specificInfo = null;
            switch (animalSpecies)
            {
                case "Bird":
                    specificInfo = "Wingspan in mtr";
                    break;
                case "Insect":
                    specificInfo = "Nr of legs";
                    break;
                case "Mammal":
                    specificInfo = "Nr of teeth";
                    break;
            }

            gbAnimalSpecification.Text = lbCategory.SelectedItem.ToString() + " specification";
            lblSpecificInfo.Text = specificInfo;
            ShowImage(animalSpecies.ToLower());
        }

        /// <summary>
        /// Shows/hides the quarantine GUI if category is not Mammal
        /// </summary>
        public void ShowHideQuarantineGUI()
        {
            Category category = AnimalManager.GetCategory(lbCategory.SelectedIndex);
            if (category == Category.Mammal)
            {
                tbQuar.Visible = true;
                lblQuar.Visible = true;
                cbxQuar.Visible = true;
                gbSpeciesInfo.Visible = true;
            }
            else
            {
                tbQuar.Visible = false;
                lblQuar.Visible = false;
                cbxQuar.Visible = false;
                gbSpeciesInfo.Visible = false;
            }
        }

        /// <summary>
        /// When user click open in file menu
        /// Shows open dialog window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuFileOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialogWindow("Open file", SerializationType.Binary);
        }

        /// <summary>
        /// When user click save in menu. 
        /// If no file has been saved open dialog window otherwise just save to the current file. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuFileSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(animalManager.FileToSave))
                SaveFileDialogWindow("Save file", SerializationType.Binary);
            else
                animalManager.SerializeToFile(animalManager.FileToSave, SerializationType.Binary);
        }

        /// <summary>
        /// When user click save as in menu.
        /// Opens dialog window for user to enter file name
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuFileSaveAs_Click(object sender, EventArgs e)
        {
            SaveFileDialogWindow("Save file as", SerializationType.Binary);
        }

        /// <summary>
        /// Close application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuFileExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// When user click new in the file menu. Clears the animal motel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuFileNew_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want a new motel?", "New file", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                InitializeGUI();
                animalManager.ClearDataBase();
            }
        }

        /// <summary>
        /// When the user chose to export as xml
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuFileXMLExport_Click(object sender, EventArgs e)
        {
            SaveFileDialogWindow("Export as XML", SerializationType.XML);
        }

        /// <summary>
        /// When user click import XML
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuFileXMLImport_Click(object sender, EventArgs e)
        {
            OpenFileDialogWindow("Import XML", SerializationType.XML);
        }

        /// <summary>
        /// Dialog window for open files 
        /// </summary>
        /// <param name="title">File title</param>
        /// <param name="type">Type of serialization</param>
        private void OpenFileDialogWindow(string title, SerializationType type)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Title = title;
            switch (type)
            { 
                case SerializationType.Binary:
                    openDialog.Filter = "Binary files|*.bin";
                    break;
                case SerializationType.XML:
                    openDialog.Filter = "XML files|*.xml";
                    break;
            }
            openDialog.ShowDialog();

            if (openDialog.FileName != string.Empty)
            {
                try
                {
                    animalManager.DeserializeFile(openDialog.FileName, type);
                    LoadDatabase();
                    if (type == SerializationType.Binary)
                        animalManager.FileToSave = openDialog.FileName;
                }
                catch (MySerializationException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        /// <summary>
        /// Dialog window for saving files
        /// </summary>
        /// <param name="title">File title</param>
        /// <param name="type">Type of serialization to be done</param>
        private void SaveFileDialogWindow(string title, SerializationType type)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Title = title;
            switch (type)
            { 
                case SerializationType.Binary:
                    saveDialog.Filter = "Binary file|*.bin";
                    break;
                case SerializationType.XML:
                    saveDialog.Filter = "XML|*.xml";
                    break;
            }
            saveDialog.ShowDialog();

            if (saveDialog.FileName != string.Empty)
            {
                string file = saveDialog.FileName;
                try
                {
                    animalManager.SerializeToFile(file, type);
                    if (type == SerializationType.Binary)
                        animalManager.FileToSave = saveDialog.FileName;
                }
                catch (MySerializationException Exception)
                {
                    MessageBox.Show(Exception.Message);
                }
            }
        }

        /// <summary>
        /// On form closing, dispose database context
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            animalManager.Dispose();
        }

        /// <summary>
        /// When selection in DataGridView is changed
        /// Loads special data into listbox (lbspecial)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void animalDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            lbSpecial.Items.Clear();
            if (animalDataGridView.CurrentRow != null)
            {
                int animalId = (int)animalDataGridView.CurrentRow.Cells[0].Value;
                string strOut = string.Format("{0}", animalManager.GetSpecialData(animalId));
                lbSpecial.Items.AddRange(strOut.Split('\n'));
            }
        }

        /// <summary>
        /// When "Show only"-checkbox is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxShowOnly_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxShowOnly.Checked)
            {
                cbShowOnly.Enabled = true;
                cbShowOnly_SelectedIndexChanged(null, null);
            }
            else
            {
                cbShowOnly.Enabled = false;
                LoadDatabase();
            }
        }

        /// <summary>
        /// Load database and connect it do datagridview
        /// Sort data by id
        /// </summary>
        private void LoadDatabase()
        {
            this.animalBindingSource.DataSource = animalManager.LoadDatabase();//context.Animals.Local.ToBindingList();
            animalBindingSource.Sort = "Id";
        }

        /// <summary>
        /// When "Show only"-dropdown is changed
        /// Gets new datasource to datagridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbShowOnly_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxShowOnly.Checked)
            {
                switch (cbShowOnly.SelectedIndex)
                {
                    case (int)Category.Bird:
                        this.animalBindingSource.DataSource = animalManager.QueryCategory(Category.Bird);
                        break;
                    case (int)Category.Insect:
                        this.animalBindingSource.DataSource = animalManager.QueryCategory(Category.Insect);
                        break;
                    case (int)Category.Mammal:
                        this.animalBindingSource.DataSource = animalManager.QueryCategory(Category.Mammal);
                        break;
                    case (int)Category.Marine:
                        this.animalBindingSource.DataSource = animalManager.QueryCategory(Category.Marine);
                        break;
                }
            }
        }

        /// <summary>
        /// Delete selected animal from database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteAnimal_Click(object sender, EventArgs e)
        {
            var currentRow = animalDataGridView.CurrentRow;
            if (currentRow.Selected == true)
            {
                int animalId = (int)animalDataGridView.CurrentRow.Cells[0].Value;
                animalManager.DeleteAnimalFromDB(animalId);
                this.animalDataGridView.ClearSelection();
            }
            else
            {
                MessageBox.Show("You have to select an animal to delete", "Delete animal");
            }
        }
    }
}