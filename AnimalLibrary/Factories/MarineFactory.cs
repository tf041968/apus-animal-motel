﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnimalMotel.DAL;

namespace AnimalLibrary
{
    /// <summary>
    /// MARINE SUB-CLASSES ARE NOT IMPLEMENTED
    /// THIS FACTORY IS ONLY CREATED TO THROW A CUSTOM EXCEPTION
    /// </summary>
    public class MarineFactory
    {
        /// <summary>
        /// Creates Marine Species
        /// </summary>
        /// <param name="marineType">Marine species as Enum</param>
        /// <param name="specification">Specifications for marine species</param>
        /// <returns>Marine object</returns>
        public static Marine CreateMarine(MarineSpecies marineType, double weight)
        {
            switch (marineType)
            { 
                default:
                    throw new SpeciesException(string.Format("{0} is not yet implemented", marineType.ToString()));          
            }
        }
    }
}
