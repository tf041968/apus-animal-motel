﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnimalMotel.DAL;

namespace AnimalLibrary
{
    public class InsectFactory
    {
        /// <summary>
        /// Creates insect species
        /// </summary>
        /// <param name="species">Type of insect to create, Enum</param>
        /// <param name="numberOfLegs">The insects number of legs as double</param>
        /// <returns>Insect object</returns>
        public static Insect CreateInsect(InsectSpecies species, double numberOfLegs)
        {
            Insect insect = null;

            switch (species)
            {
                case InsectSpecies.Bee:
                    insect = new Bee();
                    break;
                case InsectSpecies.Butterfly:
                    insect = new Butterfly();
                    break;
                default:
                    throw new SpeciesException(string.Format("Not able to create {0}", species.ToString()));
            }

            insect.Category = Category.Insect;
            insect.NumberOfLegs = numberOfLegs;

            return insect;

        }
    }
}
