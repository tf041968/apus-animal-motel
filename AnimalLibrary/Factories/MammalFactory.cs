﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnimalMotel.DAL;

namespace AnimalLibrary
{
    public class MammalFactory
    {
        /// <summary>
        /// Creates mammal species
        /// </summary>
        /// <param name="mammalType">Mammal species as enum</param>
        /// <param name="teeth">Mammals nbr of teeth as double</param>
        /// <param name="quarantineDays">Number of quarantine days for mammal, as int</param>
        /// <param name="speciesInfo">Extra information avaliable for mammal category. Bool HasRabies</param>
        /// <returns></returns>
        public static Mammal CreateMammal(MammalSpecies mammalType, double teeth, int quarantineDays, bool speciesInfo)
        {
            Mammal mammal = null;
            switch (mammalType)
            {
                case MammalSpecies.Cat:
                    mammal = new Cat(speciesInfo);
                    break;
                case MammalSpecies.Dog:
                    mammal = new Dog(speciesInfo);
                    break;
                default:
                    throw new SpeciesException("Could not create species");
                //case MammalType.Elephant:
                //    break;
                //case MammalType.Wolf:
                //    break;
            }
            if (mammal == null)
                throw new SpeciesException("Could not create species");
            else
            {
                mammal.NumberTeeth = teeth;
                mammal.QuarantineDays = quarantineDays;
                mammal.Category = Category.Mammal;
                return mammal;
            }
        }
    }
}
