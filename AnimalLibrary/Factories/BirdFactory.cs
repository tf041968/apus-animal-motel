﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnimalMotel.DAL;

namespace AnimalLibrary
{
    public class BirdFactory
    {
        /// <summary>
        /// Creates bird species
        /// </summary>
        /// <param name="birdtype">Type of bird species to create, Enum</param>
        /// <param name="wingspan">Birds wingspan as double</param>
        /// <returns>Bird object</returns>
        public static Bird CreateBird(BirdSpecies birdtype, double wingspan)
        { 
            Bird bird = null;
            switch(birdtype)
            {
                case BirdSpecies.Albatross:
                    bird = new Albatross(wingspan);
                    break;
                case BirdSpecies.Colibri:
                    bird = new Colibri(wingspan);
                    break;
            }
            bird.Category = Category.Bird;
            return bird;
        }
    }
}
