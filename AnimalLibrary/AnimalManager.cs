﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnimalMotel.DAL;
using UtilitiesLibrary;
using System.Data.Entity;
using System.ComponentModel;

namespace AnimalLibrary
{
    /// <summary>
    /// CLASS WITH HELPER METHODS
    /// </summary>
    public class AnimalManager : IDisposable
    {
        private int latestCategory;
        private string fileToSave;
        private AnimalDbContext db = new AnimalDbContext(); 
        /// <summary>
        /// Constructor
        /// </summary>
        public AnimalManager()
        {
            //db.Database.Initialize(false);
            Database.SetInitializer(new ProductionSeedingStrategy());
        }

        /// <summary>
        /// Get gender from an int.
        /// </summary>
        /// <param name="selectedGender">listbox position to get</param>
        /// <returns>gender at selectedgender position</returns>
        public static Gender GetGender(int selectedGender)
        {
            Gender gender = (Gender)selectedGender;
            return gender;
        }
        
        /// <summary>
        /// Get category from an int
        /// Takes selected row as int. 
        /// </summary>
        /// <param name="selectedCategory"></param>
        /// <returns>Return category at selected index position</returns>
        public static Category GetCategory(int selectedCategory)
        {
            Category currentCategory = (Category)selectedCategory;
            return currentCategory;
        }

        /// <summary>
        /// Add animal to database
        /// </summary>
        /// <param name="animal"></param>
        public void AddAnimal(Animal animal)
        {
            using (var db = new AnimalDbContext())
            {
                db.Animals.Add(animal);
                db.SaveChanges();
            } 
        }

        /// <summary>
        /// Add range of animals
        /// </summary>
        /// <param name="animalList">List with animals to add</param>
        public void AddAnimalRange(List<Animal> animalList)
        {
            using (var db = new AnimalDbContext())
            {
                db.Animals.AddRange(animalList);
                db.SaveChanges();
            } 
        }

        /// <summary>
        /// Property to get list of all added animals. 
        /// </summary>
        public List<Animal> GetAnimalList
        {
            get { return db.Animals.ToList(); }
        }

        /// <summary>
        /// Get all species enums and add them to a list. 
        /// Sorts the list aphabetically
        /// </summary>
        /// <returns>List of all species</returns>
        public static List<Enum> GetEnumList()
        {
            List<Enum> enumList = new List<Enum>();
            enumList.AddRange(Enum.GetValues(typeof(BirdSpecies)).Cast<Enum>());
            enumList.AddRange(Enum.GetValues(typeof(MammalSpecies)).Cast<Enum>());
            enumList.AddRange(Enum.GetValues(typeof(InsectSpecies)).Cast<Enum>());
            // COMMENTED OUT THE 2 ROWS BELOW SINCE THEY ARE NOT IMPLEMENTED (YET!?)
            //enumList.AddRange(Enum.GetValues(typeof(MarineSpecies)).Cast<Enum>());
            //enumList.AddRange(Enum.GetValues(typeof(ReptileSpecies)).Cast<Enum>());
            enumList.Sort((a, b) => string.Compare(a.ToString(), b.ToString()));
            return enumList;
        }

        /// <summary>
        /// Deserialize file and add objects to list
        /// Sets id to continue after the last deserialized object. 
        /// </summary>
        /// <param name="file">Filename to deserialize</param>
        public void DeserializeFile(string file, SerializationType type)
        {
            ClearDataBase();
            switch (type)
            {
                case SerializationType.Binary:
                    AddAnimalRange(Serialization.BinaryDeserializeFile<List<Animal>>(file));
                    break;
                case SerializationType.XML:
                    Type[] derivedClasses = GetSubClasses(typeof(Animal));
                    AddAnimalRange(Serialization.XMLDeserialization<List<Animal>>(file, derivedClasses));
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Serialize current animallist to file
        /// </summary>
        /// <param name="fileName">Filename to save to</param>
        public void SerializeToFile(string fileName, SerializationType type)
        {
            switch (type)
            { 
                case SerializationType.Binary:
                    Serialization.BinarySerializeToFile(fileName, GetAnimalList);
                    break;
                case SerializationType.XML:
                    Type[] alltypes = GetSubClasses(typeof(Animal));
                    Serialization.XMLSerialization(fileName, GetAnimalList, alltypes);
                    break;
            }
        }

        /// <summary>
        /// Get all subclasses of baseclass
        /// </summary>
        /// <param name="baseClass">Baseclass to get subclasses from</param>
        /// <returns>Type array with subclasses</returns>
        private Type[] GetSubClasses(Type baseClass)
        {
            return System.Reflection.Assembly.GetAssembly(baseClass).GetTypes().Where(_type => _type.IsSubclassOf(baseClass)).ToArray();
        }

        /// <summary>
        /// Dispose the db context
        /// </summary>
        public void Dispose()
        {
            db.Dispose();
        }

        /// <summary>
        /// Save changes to database
        /// </summary>
        public void SaveToDatabase()
        {
            db.SaveChanges();
        }

        /// <summary>
        /// Load data from database
        /// </summary>
        /// <returns>Loaded data as a binding list</returns>
        public BindingList<Animal> LoadDatabase()
        {
            db.Animals.Load();
            return db.Animals.Local.ToBindingList();
        }

        /// <summary>
        /// Make a query to the database to filter category
        /// </summary>
        /// <param name="category">Category to get data from</param>
        /// <returns>Filtered data as list.</returns>
        public IEnumerable<Animal> QueryCategory(Category category)
        {
            db.Animals.Load();
            var q = from x in db.Animals.Local
                    where x.Category == category
                    select x;
            return q.ToList();
        }

        /// <summary>
        /// Get special data for animal
        /// </summary>
        /// <param name="animalIndex">Id of animal to get data from</param>
        /// <returns>Special data from selected animal</returns>
        public string GetSpecialData(int animalIndex)
        {
            return db.Animals.Find(animalIndex).GetAnimalSpecificData();
        }

        /// <summary>
        /// Clear all tables in the database. 
        /// </summary>
        public void ClearDataBase()
        {
            db.Animals.Load();
            var ani = db.Animals;
            db.Animals.RemoveRange(ani);
            db.SaveChanges();
        }

        /// <summary>
        /// Delete animal from database
        /// </summary>
        /// <param name="animalIndex">Animals identity in database</param>
        public void DeleteAnimalFromDB(int animalIndex)
        {
            var animal = db.Animals.Find(animalIndex);
            db.Animals.Remove(animal);
            db.SaveChanges();
        }

        /// <summary>
        /// Get enums
        /// </summary>
        /// <param name="type">Type of enum to get</param>
        /// <returns>Array containing enum values</returns>
        public Array GetEnum(Type type)
        {
            return Enum.GetValues(type);
        }

        /// <summary>
        /// Create animal from user input
        /// </summary>
        /// <param name="categoryIndex">Selected category index</param>
        /// <param name="animalIndex">selected animal index</param>
        /// <param name="specificationInfo">Specification data</param>
        /// <param name="quarantineDays">Quarantinedays data</param>
        /// <param name="checkBox">Checkbox true/false</param>
        public void CreateAnimal(int categoryIndex, int animalIndex, double specificationInfo, int quarantineDays, bool checkBox)
        {
            animal = null;
            try
            {
                switch (GetCategory(categoryIndex))
                {
                    case Category.Mammal:
                        MammalSpecies mammalSpecies = (MammalSpecies)animalIndex;
                        animal = MammalFactory.CreateMammal(mammalSpecies, specificationInfo, quarantineDays, checkBox);
                        break;
                    case Category.Bird:
                        BirdSpecies birdSpecies = (BirdSpecies)animalIndex;
                        animal = BirdFactory.CreateBird(birdSpecies, specificationInfo);
                        break;
                    case Category.Insect:
                        InsectSpecies insectSpecies = (InsectSpecies)animalIndex;
                        animal = InsectFactory.CreateInsect(insectSpecies, specificationInfo);
                        break;
                    //ONLY ADDED TO SHOW CUSTOM EXCEPTIONS
                    case Category.Marine:
                        MarineSpecies marineSpecies = (MarineSpecies)animalIndex;
                        animal = MarineFactory.CreateMarine(marineSpecies, specificationInfo);
                        break;
                    //HAS NOT IMPLEMENTED ANIMALS FOR THESE CATEGORIES YET
                    //COMMENTED OUT CODE BELOW IN CASE I WANT TO INPLEMENT IT IN THE FUTURE
                    //case Category.Reptile:
                    //    ReptileSpecies reptileSpecies = (ReptileSpecies)currentIndex;
                    //    break;
                }
            }
            catch (SpeciesException)
            {
                throw;
            }
        }

        /// <summary>
        /// Set standard data for animal
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="age">Age</param>
        /// <param name="gender">Gender</param>
        /// <param name="importantInfo">Inmportant info</param>
        public void SetStandardData(string name, int age, Gender gender, string importantInfo)
        {
            try
            {
                animal.Name = name;
                animal.Age = age;
                animal.Gender = gender;
                animal.AnimalImportantInfo = importantInfo;
                AddAnimal(animal);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// PROPERTIES ///
        public Animal animal { get; set; }
        public int LatestCategory
        {
            get { return latestCategory; }
            set { latestCategory = value; }
        }

        public string FileToSave
        {
            get { return fileToSave; }
            set { fileToSave = value; }
        }
    }
}
