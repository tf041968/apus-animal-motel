﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalLibrary
{
    /// <summary>
    /// Custom exception to be thrown if Species is not possible to create
    /// Contains various constructors depending on what message is to be thrown
    /// </summary>
    [Serializable]
    public class SpeciesException : Exception
    {
        public SpeciesException() { }
        public SpeciesException(string message) : base(message) { }
        public SpeciesException(string message, Exception inner) : base(message, inner) { }
        protected SpeciesException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
