﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnimalMotel.DAL
{
    [Serializable]
    [Table("Bee")]
    public class Bee : Insect
    {
        private int amountHoneyProduced;

        /// <summary>
        /// Constructor
        /// </summary>
        public Bee()
        { }

        /// <summary>
        /// Constructor with argument
        /// </summary>
        /// <param name="amountHoneyProduced">Int, amount honey the bee produce</param>
        public Bee(int amountHoneyProduced)
        { }

        /// <summary>
        /// Property for amountOfHoneyProduced
        /// </summary>
        public int AmountHoneyProduced
        {
            get { return amountHoneyProduced; }
            set { amountHoneyProduced = value; }
        }

        /// <summary>
        /// Create a unique ID
        /// </summary>
        /// <returns></returns>
        public override string GetAnimalId()
        {
            return CatName;
        }

        /// <summary>
        /// Get Animal specific data
        /// </summary>
        /// <returns>Animal specific data as string</returns>
        public override string GetAnimalSpecificData()
        {
            string strOut = AnimalImportantInfo + Environment.NewLine;
            strOut += string.Format("Has {0} legs.", NumberOfLegs) + Environment.NewLine;
            strOut += string.Format("Produce: {0} grams of honey each day", amountHoneyProduced) + Environment.NewLine;
            return strOut;
        }
    }
}
