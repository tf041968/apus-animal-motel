﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnimalMotel.DAL
{
    public enum InsectSpecies
    {
        Bee,
        Butterfly,
    }

    [Serializable]
    /// Do not need XmlInclude since i use extratypes in xml de-/serializer
    //[XmlInclude(typeof(Bee))]
    //[XmlInclude(typeof(Butterfly))]
    /// <summary>
    /// Abstract class for INSECT
    /// </summary>
    [Table("Insect")]
    public abstract class Insect : Animal
    {
        private double numberOfLegs;

        /// <summary>
        /// Constructor
        /// </summary>
        public Insect()
        { }

        /// <summary>
        /// Property for numberOfLegs
        /// </summary>
        public double NumberOfLegs
        {
            get { return numberOfLegs; }
            set { numberOfLegs = value; }
        }

        /// <summary>
        /// Property for creating a unique ID
        /// </summary>
        public string CatName
        {
            get { return "Insc"; }
        }
    }
}
