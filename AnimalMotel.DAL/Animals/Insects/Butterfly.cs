﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnimalMotel.DAL
{
    [Serializable]
    [Table("Butterfly")]
    public class Butterfly : Insect
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Butterfly()
        { }

        /// <summary>
        /// Create a unique id
        /// </summary>
        /// <returns></returns>
        public override string GetAnimalId()
        {
            return CatName;
        }
        
        /// <summary>
        /// Get Animal specific data
        /// </summary>
        /// <returns>Animal specific data as string</returns>
        public override string GetAnimalSpecificData()
        {
            string strOut = AnimalImportantInfo + Environment.NewLine;
            strOut = (string.IsNullOrEmpty(NumberOfLegs.ToString()))
                ? string.Format("Has no legs") : string.Format("Has {0} legs", NumberOfLegs) + Environment.NewLine;
            return strOut;
        }
    }
}
