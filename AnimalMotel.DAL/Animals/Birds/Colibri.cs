﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnimalMotel.DAL
{
    [Serializable]
    [Table("Colibri")]
    public class Colibri : Bird
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Colibri()
        { }

        /// <summary>
        /// Constructor with argument
        /// </summary>
        /// <param name="wingspan">Double, birds wingspan</param>
        public Colibri(double wingspan)
            : base(wingspan)
        { }

        /// <summary>
        /// Set species data
        /// </summary>
        /// <returns>String with species data</returns>
        public override string SetStinkData()
        {
            string strOut = "The Colibri is a non-stinking bird";
            return strOut;
        }

        /// <summary>
        /// Property to create unique id
        /// </summary>
        /// <returns></returns>
        public override string GetAnimalId()
        {
            return CatName;
        }

        /// <summary>
        /// Get AnimalSpecificData
        /// </summary>
        /// <returns>String with animalSpecificData</returns>
        public override string GetAnimalSpecificData()
        {
            string strOut = strOut = AnimalImportantInfo + Environment.NewLine;
            strOut += SetStinkData() + Environment.NewLine;
            strOut += "Wingspan: " + Wingspan + "M" + Environment.NewLine;
            return strOut;
        }
    }
}
