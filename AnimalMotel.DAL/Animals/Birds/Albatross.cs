﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnimalMotel.DAL
{
    [Serializable]
    [Table("Albatross")]
    public class Albatross : Bird
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Albatross()
        { }

        /// <summary>
        /// Constructor with argument
        /// </summary>
        /// <param name="wingspan">Double, birds wingspan</param>
        public Albatross(double wingspan)
            : base(wingspan)
        { }

        /// <summary>
        /// Set species data
        /// </summary>
        /// <returns>String with species data</returns>
        public override string SetStinkData()
        {
            string strOut = "The Albatross stinks fish. Yuck!";
            return strOut;
        }

        /// <summary>
        /// Creates a unique id
        /// </summary>
        /// <returns></returns>
        public override string GetAnimalId()
        {
            return CatName;
        }

        /// <summary>
        /// Get Animal specific data
        /// </summary>
        /// <returns>Animal specific data as string</returns>
        public override string GetAnimalSpecificData()
        {
            string strOut = AnimalImportantInfo + Environment.NewLine;
            strOut += SetStinkData() + Environment.NewLine;
            strOut += string.Format("Wingspan: {0} m", Wingspan) + Environment.NewLine;
            return strOut;
        }
    }
}
