﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnimalMotel.DAL
{
    public enum BirdSpecies
    {
        Albatross,
        Colibri
    }
    [Serializable]
    /// Do not need XmlInclude since i use extratypes in xml de-/serializer
    //[XmlInclude(typeof(Albatross))]
    //[XmlInclude(typeof(Colibri))]
    [Table("Bird")]
    public abstract class Bird : Animal
    {
        private double wingspan;

        /// <summary>
        /// Constructor
        /// </summary>
        public Bird()
        { }

        /// <summary>
        /// Constructor with argument
        /// </summary>
        /// <param name="wingspan">Double, birds wingspan</param>
        public Bird(double wingspan)
        {
            this.wingspan = wingspan;
        }
        
        /// <summary>
        /// Propertie to create unique id
        /// </summary>
        public string CatName
        {
            get { return "Bird"; }
        }

        /// <summary>
        /// Property for wingspan
        /// </summary>
        public double Wingspan
        {
            get { return wingspan; }
            set { wingspan = value;}
        }

        /// <summary>
        /// Get Animal specific data
        /// </summary>
        /// <returns>Animal specific data as string</returns>
        public abstract string SetStinkData();
    }
}
