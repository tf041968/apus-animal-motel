﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnimalMotel.DAL
{
    public enum MammalSpecies
    {
        Cat,
        Dog,
    }

    [Serializable]
    /// Do not need XmlInclude since i use extratypes in xml de-/serializer
    //[XmlInclude(typeof(Cat))]
    //[XmlInclude(typeof(Dog))]
    /// <summary>
    /// Abstract class for MAMMAL
    /// </summary>
    [Table("Mammal")]
    public abstract class Mammal : Animal
    {
        private double numberTeeth;
        private int quarantineDays;
        
        /// <summary>
        /// Constructor
        /// </summary>
        public Mammal()
        { }

        /// <summary>
        /// Constructor with 2 arguments
        /// </summary>
        /// <param name="teeth">Double, nbr of teeth the mammal has</param>
        /// <param name="quarantineDays">Int, nbr of days the mammal shall be in quarantine</param>
        public Mammal(double teeth, int quarantineDays)
        {
            this.numberTeeth = teeth;
            this.quarantineDays = quarantineDays;
        }

        //Property for numberOfTeeth
        public double NumberTeeth
        {
            get { return numberTeeth; }
            set { numberTeeth = value;  }
        }

        /// <summary>
        /// Property for quarantineDays
        /// </summary>
        public int QuarantineDays
        {
            get { return quarantineDays; }
            set { quarantineDays = value;  }
        }

        /// <summary>
        /// Property for creating a unique ID
        /// </summary>
        public string CatName
        {
            get { return "Maml"; }  
        }

        
        /// <summary>
        /// Get Animal specific data
        /// </summary>
        /// <returns>Animal specific data as string</returns>
        public abstract string SetSpecificData();
    }
}
