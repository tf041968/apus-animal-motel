﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnimalMotel.DAL
{
    [Serializable]
    [Table("Cat")]
    public class Cat : Mammal
    {
        private bool hasNoTail;
        
        /// <summary>
        /// Constructor
        /// </summary>
        public Cat()
        { }

        /// <summary>
        /// constructor with argument
        /// </summary>
        /// <param name="hasNoTail">Bool, if the cat has a tail or not</param>
        public Cat(bool hasNoTail)
        {
            this.hasNoTail = hasNoTail;
        }

        /// <summary>
        /// Constructor with 3 arguments
        /// </summary>
        /// <param name="hasNoTail">Bool, if the cat has a tail or not</param>
        /// <param name="teeth">Double, nbr of teeth the cat has</param>
        /// <param name="quarantineDays">Int, nbr of days the cat shall be in quarantine</param>
        public Cat(bool hasNoTail, double teeth, int quarantineDays)
            : base(teeth,quarantineDays)
        {
            this.hasNoTail = hasNoTail;
        }

        /// <summary>
        /// Property for hasNoTail
        /// </summary>
        public bool HasNoTail
        {
            get { return hasNoTail; }
            set { hasNoTail = value; }
        }

        /// <summary>
        /// Get Animal specific data
        /// </summary>
        /// <returns>Animal specific data as string</returns>
        public override string GetAnimalSpecificData()
        {
            
            string strOut = AnimalImportantInfo + Environment.NewLine;
            strOut += (string.IsNullOrEmpty(QuarantineDays.ToString()) ?
                string.Empty : "In quarantine for " + QuarantineDays + " days" + Environment.NewLine);
            strOut += (string.IsNullOrEmpty(NumberTeeth.ToString())) ? "No info about our nr. of teeth"
                : "Nr. of teeth: " + NumberTeeth + Environment.NewLine;
            if (hasNoTail)
                strOut += "This cat has no tail" + Environment.NewLine;
            strOut += SetSpecificData();
            return strOut;
        }

        /// <summary>
        /// Property for creating a unique ID
        /// </summary>
        /// <returns></returns>
        public override string GetAnimalId()
        {
            return CatName;
        }

        /// <summary>
        /// Property for setting specific species data
        /// </summary>
        /// <returns>String of specific secies data</returns>
        public override string SetSpecificData()
        {
            return "Cats are so cosy and cuddly";
        }

        //public int MammalID { get; set; }
        //public virtual Mammal Mammal { get; set; }
    }
}
