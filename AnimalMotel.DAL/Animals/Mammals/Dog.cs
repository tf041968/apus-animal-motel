﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnimalMotel.DAL
{
    [Serializable]
    [Table("Dog")]
    public class Dog : Mammal
    {
        private bool hasRabies;
        
        /// <summary>
        /// Constructor
        /// </summary>
        public Dog()
        { }

        /// <summary>
        /// Constructor with argument
        /// </summary>
        /// <param name="hasRabies">Bool, if the dog has rabies or not</param>
        public Dog(bool hasRabies)
        {
            this.hasRabies = hasRabies;
        }

        /// <summary>
        /// Constructor with 3 arguments
        /// </summary>
        /// <param name="hasRabies">Bool, if the dog has rabies or not</param>
        /// <param name="teeth">Double, the nbr of teeth the dog has. </param>
        /// <param name="quarantineDays">Int, Nbr of days the dog shall be in quarantine</param>
        public Dog(bool hasRabies, double teeth, int quarantineDays): base (teeth, quarantineDays)
        {
            this.hasRabies = hasRabies;
        }

        /// <summary>
        /// Property for hasRabies
        /// </summary>
        public bool HasRabies
        {
            get { return hasRabies; }
            set { hasRabies = value; }
        }

        /// <summary>
        /// Get Animal specific data
        /// </summary>
        /// <returns>Animal specific data as string</returns>
        public override string GetAnimalSpecificData()
        {
            string rabies = string.Empty;
            string strOut = AnimalImportantInfo + Environment.NewLine;
            strOut += (string.IsNullOrEmpty(QuarantineDays.ToString()) ? 
                string.Empty : "In quarantine for " + QuarantineDays + " days" + Environment.NewLine);
            strOut += (string.IsNullOrEmpty(NumberTeeth.ToString())) ? "No info about our nr. of teeth"
                : "Nr. of teeth: " + NumberTeeth + Environment.NewLine;
            if(!HasRabies)
                rabies = "not ";
            strOut += "The dog has " + rabies + "rabies" +  Environment.NewLine;
            strOut += SetSpecificData() + Environment.NewLine;
            return strOut;
        }

        /// <summary>
        /// Property for creating a UNique id
        /// </summary>
        /// <returns></returns>
        public override string GetAnimalId()
        {
            return CatName;
        }

        /// <summary>
        /// Property for setting specific species data
        /// </summary>
        /// <returns>String of specific secies data</returns>
        public override string SetSpecificData()
        {
           string strOut= "Remember, it's a dog. It needs to be walked a couple of times a day!";
           return strOut;
        }

        //public int MammalID { get; set; }
        //public virtual Mammal Mammal { get; set; }
    }
}
