﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace AnimalMotel.DAL
{
    /// <summary>
    /// Database context class
    /// </summary>
    public class AnimalDbContext : DbContext
    {
        public DbSet<Animal> Animals { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public AnimalDbContext() : base("AnimalDbContext")
        {
        }

        /// <summary>
        /// Static constructor. 
        /// </summary>
        static AnimalDbContext()
        {
            Database.SetInitializer<AnimalDbContext>(new ProductionSeedingStrategy());
        }
    }

    /// <summary>
    /// Class for creating database seed
    /// </summary>
    public class ProductionSeedingStrategy : DropCreateDatabaseIfModelChanges<AnimalDbContext>
    {
        /// <summary>
        /// Sets seed in databasecontext
        /// </summary>
        /// <param name="context"></param>
        protected override void Seed(AnimalDbContext context)
        {
            var animals =
                new List<Animal>()
                { 
                    new Dog()
                    {
                        Name = "Pluto", 
                        Category = Category.Mammal, 
                        Gender = Gender.Female, 
                        Age=3,
                        QuarantineDays = 4,
                        NumberTeeth = 22
                    },
                    new Cat()
                    {
                        Name = "Stuart", 
                        Category = Category.Mammal, 
                        Gender = Gender.Female, 
                        Age=20
                    },
                    new Colibri()
                    {
                        Name="Tinky", 
                        Category = Category.Bird, 
                        Gender = Gender.Male, 
                        Age=12, 
                        Wingspan=2
                    },
                    new Albatross()
                    {
                        Name="Boris", 
                        Category = Category.Bird, 
                        Gender = Gender.Unknown,
                        AnimalImportantInfo = "Boris does not like fish"
                    },
                    new Bee()
                    {
                        Name="Stinger", 
                        Category = Category.Insect, 
                        Gender = Gender.Unknown,
                    },
                    new Butterfly()
                    {
                        Name="Daisy", 
                        Category = Category.Insect, 
                        Gender = Gender.Male, 
                        Age=88
                    }
        };
            context.Animals.AddRange(animals);
            context.SaveChanges();
       }
    }
}