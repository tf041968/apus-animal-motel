﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnimalMotel.DAL
{
    [Serializable]
    /// Do not need XmlInclude since i use extratypes in xml de-/serializer
    //[XmlInclude(typeof(Bird))]
    //[XmlInclude(typeof(Mammal))]
    //[XmlInclude(typeof(Insect))]
    [Table("Animals")]
    public abstract class Animal : IAnimal
    {
        private int id;
        private double age;
        private string name;
        private Gender gender;
        private Category category;
        private string animalImportantInfo;
        public abstract string GetAnimalId();
        public abstract string GetAnimalSpecificData();

        public Animal()
        { }

        public int Id { get { return id; } set { id = value;} }

        /// <summary>
        /// Property for age
        /// </summary>
        public double Age
        {
            get { return age; }
            set { age = value; }
        }

        /// <summary>
        /// Property for gender
        /// </summary>
        public Gender Gender
        { 
            get { return gender; }
            set { gender = value;}
        }

        /// <summary>
        /// Property for category
        /// </summary>
        public Category Category
        {
            get { return category; }
            set { category = value; }
        }
        
        /// <summary>
        /// Property for name
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// Property for animalImportantInfo
        /// </summary>
        public string AnimalImportantInfo
        {
            get { return animalImportantInfo; }
            set {
                if (string.IsNullOrEmpty(value))
                    animalImportantInfo = "Has no important information";
                else
                    animalImportantInfo = value;
            }
        }
    }
}
