﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalMotel.DAL
{
    public enum Gender
    {
        Female,
        Male,
        Unknown
    }
}
