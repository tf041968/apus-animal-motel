﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalMotel.DAL
{
    public interface IAnimal
    {
        int Id { get; set; }
        double Age { get; set; }
        Gender Gender { get; set; }
        Category Category { get; set; }
        string GetAnimalSpecificData();
    }
}
